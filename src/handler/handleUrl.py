import asyncio
import re
import requests
from decouple import config
from bs4 import BeautifulSoup, Comment
from scrapegraphai.graphs import SmartScraperGraph
from scrapegraphai.utils import prettify_exec_info
import nest_asyncio
nest_asyncio.apply()
# from extractnet import Extractor
from fake_useragent import UserAgent
ua = UserAgent()

from parser.scrapeXasync import scrape_tweet

from logging import getLogger, DEBUG, INFO


logger = getLogger(__name__)  # Create a logger object for the handleURL module
logger.setLevel(DEBUG)  # Set the default log level to DEBUG

async def handleURL(url):
    """Handles various scraping strategies based upon the website"""
    body = None
    # Check if URL is from Twitter or X.com
    if re.match(r'^https?://(?:www\.)?(twitter|x)\.com', url):
        # print("add me back in")
        body = await  scrape_tweet(url)  # Call the scrape_tweet function with the URL
    else:
        body = await fetchURL(url)

    return body
  

 
def fetch_webpage(url):
    print(f'Fetching.... {url}')
    
    response = requests.get(url, headers={
        'User-Agent': ua.random,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Language': 'en-US,en;q=0.9',
        'Referer': 'https://www.google.com/',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'Accept-Encoding': 'gzip, deflate, br'
    })
    
    logger.debug(response.status_code)
    
    if response.status_code == 200:
        return response.content
    else:
        logger.error(f'Failed to fetch webpage, status code: {response.status_code}')
        return None

def remove_unwanted_elements(soup):
    # Remove script, style, and meta elements
    for element in soup.find_all(['script', 'style', 'meta']):
        if element:
            # logger.debug(f"Removing element: <{element.name}> with attributes {element.attrs}")
            element.decompose()

    # Remove comments
    for comment in soup.find_all(string=lambda text: isinstance(text, Comment)):
        if comment:
            # logger.debug("Removing comment")
            comment.extract()
            
def remove_irrelevant_elements(soup, irrelevant_patterns):
    # Remove elements based on patterns
    for pattern in irrelevant_patterns:
        for element in soup.find_all(class_=lambda class_: class_ and pattern in class_.lower()):
            logger.debug(f"Removing element: <{element.name}> with attributes {element.attrs}")
            element.decompose()
        for element in soup.find_all(id=lambda id_: id_ and pattern in id_.lower()):
            logger.debug(f"Removing element: <{element.name}> with attributes {element.attrs}")
            element.decompose()

def clean_body_text(body):
    if body:
        body_text = body.get_text()
        cleaned_text = '\n'.join([line for line in body_text.split('\n') if line.strip() != ''])
        cleaned_text = ' '.join([word for word in cleaned_text.split(' ') if word.strip() != ''])
        return cleaned_text
    return ''

async def fetchURL(url, do_remove_irrelevant = True):
    page_content = fetch_webpage(url)
    if page_content:
        soup = BeautifulSoup(page_content, 'html.parser')

        remove_unwanted_elements(soup)

        if do_remove_irrelevant:
            irrelevant_patterns = [
                'footer', 'nav', 'menu', 'ad__wrapper', 'promo', 'related', 'suggested', 'sponsor', 'social', 'share', 'sharing', 'search', 'follow', 
                'skip-link', 'flag', 'heading', 'screen-reader-text', 'email-response', 'more-stories', 'partner-article-links', 'widget_nypost_pagesix_rss_widget', 'recirc__headline', 'breaking-news', # NYPOST
                'beta', 'watermark', 'weather', 'supplements', 'puff'    # dailymail.co.uk
                ]
            remove_irrelevant_elements(soup, irrelevant_patterns)

        body = soup.body
        response = clean_body_text(body)
        logger.debug(response)
        print(response)
        logger.debug(f"response length: {len(response)}")
        
        if(len(response) < 5 and do_remove_irrelevant):
            logger.debug("body is empty... retry")
            response = await fetchURL(url, False)
            logger.debug(f"Did we get more text? {len(response) > 5}")
            # logger.debug(response)
        
        return response

    return "Error: Failed to retrieve or parse the webpage."


graph_config = {
   "llm": {
      "model": f"ollama/{config('OPENAI_MODEL_NAME')}",
      "temperature": 1,
      "format": "json",  # Ollama needs the format to be specified explicitly
      "model_tokens": 8000, #  depending on the model set context length
      "base_url": "http://localhost:11434",  # set ollama URL of the local host (YOU CAN CHANGE IT, if you have a different endpoint
   },
   "embeddings": {
      "model": "ollama/nomic-embed-text",
      "temperature": 0,
      "base_url": "http://localhost:11434",  # set ollama URL
   }
}

async def smartScrape(url):
    smart_scraper_graph = SmartScraperGraph(
        prompt="Provide the complete main content article from this webpage.",
        # also accepts a string with the already downloaded HTML code
        source=url,
        config=graph_config
    )

    result = smart_scraper_graph.run()
    return result


# async def httpExtraction(url):
#     print('Extracting.... ' + url)

#     extraction = ""
#     try:
#         # Fetch the webpage contents
#         page = requests.get(url, {'User-Agent': 'Mozilla/5.0'})

#         if page.status_code == 200:
#             extraction = Extractor().extract(page.text)
#             try:
#                 extraction['date'] = extraction['date'].strftime('%Y-%m-%d %H:%M:%S')
#             except Exception as e:
#                 print(e)
#                 print("cleansing failed... carry on.")

        
#             extraction = json.dumps(str(extraction), skipkeys=True)

#         else:
#             print(f'Extraction error code: {page.status_code}')
#     except Exception as e:
#         extraction = f"Error occurred while extracting webpage content: {str(e)}"
        
#     print("==== extraction ====")
#     print(extraction)

#     return extraction

if __name__ == '__main__':
    asyncio.run(fetchURL('https://www.dailymail.co.uk/news/article-13592257/kamala-harris-tweaks-schedule-biden-july-fourth.html?ns_mchannel=rss&ns_campaign=1490&ito=1490'))
    # fetchURL()