import json
from telegram import Update
from telegram.ext import ContextTypes

from crew.searchCrew import askSearchCrew
from crew.telegramCrew import askTelegramCrew
from db.message import insert_message, message_processed
from handler.dspy_summary import dspy_summary
from handler.handleUrl import handleURL
from handler.fabricHandler import check_helper_tools, download_youtube_transcript, is_fabric_available, call_fabric_command, is_youtube_url
from parser.parseMessageEntities import parseMessageEntities



async def fetchURLDocuments(urls):
    """Fetch the documents from URLs."""
    documents = []

    for url in urls:
        body = await handleURL(url)
        documents.append(f'URL: {url}\n{body}\n') 
    
    if len(documents) == 0:
        documents = ""
    else:
        joined_documents = "\n".join(documents)
        documents = "\n\nPresented below is a collection of articles retrieved from various URLs, processed, and organized into a JSON format. This information is not a query but a dataset for summarization. Please analyze the content systematically and provide a concise summary of the key information, findings, and insights extracted from these articles. The objective is to distill the core themes, notable facts, and any recurring patterns or important anomalies identified within the dataset. Approach this task as a comprehensive synthesis of the data provided, rather than responding to a user's question.\n" + joined_documents

    return documents




async def debug(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Debug the message."""
    print("debug")
    print(update.message) # message is a telegram.MessageEntity

    await update.message.reply_text("debug...")




async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /help is issued."""
    
    help_text = """LLM Bot here to assist.
Forward me a message with URL attached and I will scrape and summarize the content.   

/fabric to interact with Fabric AI - https://github.com/danielmiessler/fabric

## Fabric Bot commands

All flags are passed to fabric cli

examples:
```
/fabric -p extract_wisdom But of all the things the ancients left us, I believe the best thing was their words of wisdom.
/fabric -l
/fabric --listmodels
/fabric -p extract_wisdom https://www.youtube.com/watch?v=dQw4w9WgXcQ
```
"""
    await update.message.reply_text(help_text)


async def llm(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Engage the LLM"""
    print("llm")
    print(update.message) # message is a telegram.MessageEntity
    
    urls = parseMessageEntities(update.message.entities, update.message)

    documents = await fetchURLDocuments(urls)

    content = f'{update.message.text}{documents}'

    print("==== content ====")
    print(content)

    response = askTelegramCrew(content)

    await update.message.reply_text(response)

async def crew(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Use the crew to respond to the user"""
    print(update.message)

    message_urls = parseMessageEntities(update.message.entities, update.message)
    caption_urls = parseMessageEntities(update.message.caption_entities, update.message)
    urls = message_urls + caption_urls
    caption = update.message.caption
    text = update.message.text

    message_id = await insert_message(context.application.db_pool, update.message)

    # async with context.application.db_pool.acquire() as conn:
    #     message_id = await conn.fetchval('''
    #     INSERT INTO messages (text , caption, processed ) VALUES ($1,$2, $3)
    #     RETURNING id;
    #                        ''', text, caption, False)

    response, title_task, summary_task, clickbait_task = askTelegramCrew(caption, text, urls)
    
    print("\n\t=== title task ===\r")
    print(title_task)

    await message_processed(context.application.db_pool, message_id)
    print(f"{message_id} processed successfully")
    # print("==== crew response ====")
    # print(response)

    await update.message.reply_text(response, reply_to_message_id=update.message.id)

async def searchCrew(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Use SearchCrew to search for the user"""

    # Check if message has text
    response = f"Unable to search for: {update.message.text}"
    
    if(len(update.message.text) > 5):
        response = askSearchCrew(update.message.text)

    await update.message.reply_text(response)
    
async def dspySummarize(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Use dspy summarize pipeline"""
    
    print(update.message)
    
    message_urls = parseMessageEntities(update.message.entities, update.message)
    caption_urls = parseMessageEntities(update.message.caption_entities, update.message)
    urls = message_urls + caption_urls
    
    content = None
    message_id = await insert_message(context.application.db_pool, update.message)
    
    if is_youtube_url(urls[0]):
        transcript = download_youtube_transcript(urls[0])
        print("Transcript:")
        print(transcript)
        if transcript:
            content = transcript
    else:
        content = await handleURL(urls[0])
    
    response = await dspy_summary(content)
    
    await message_processed(context.application.db_pool, message_id)
    print(f"{message_id} processed successfully")

    await update.message.reply_text(f"""{response.summary}""")

    

async def fabric_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Handle Fabric AI commands"""

    if not is_fabric_available():
        await update.message.reply_text("Fabric AI is not available on this system.")
        return

    # Check if the message and text are not None
    if update.message is None or update.message.text is None:
        await update.message.reply_text("No command provided. Please provide a Fabric command to execute.")
        return

    command_parts = update.message.text.strip().split(maxsplit=2)
    if len(command_parts) < 2:
        await update.message.reply_text("Please provide a Fabric command to execute.")
        return

    fabric_command = command_parts[1]
    fabric_args = []
    text_input = ""

    if len(command_parts) == 3:
        # If there's a third part, it could be args or text input
        if fabric_command.startswith("-"):
            # If command has a flag
            fabric_args = command_parts[2].split(maxsplit=1)
            if len(fabric_args) == 2:
                text_input = fabric_args[1]
                fabric_args = [fabric_args[0]]
        else:
            text_input = command_parts[2]
            
    available_tools = check_helper_tools()
    
    print(f'debug {text_input} {available_tools}')
    
    if 'yt' in available_tools and is_youtube_url(text_input):
        youtube_url = text_input.split()[-1]
        transcript = download_youtube_transcript(youtube_url)
        print("Transcript:")
        print(transcript)
        if transcript:
            text_input = transcript

    result = call_fabric_command(fabric_command, text_input, *fabric_args)
    
    if result:
        await update.message.reply_text(f"Fabric command result:\n{result}")
    else:
        await update.message.reply_text("Failed to execute Fabric command.")
