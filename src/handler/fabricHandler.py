import subprocess, os

import requests

def is_fabric_available():
    """Check if Fabric CLI is available."""
    try:
        result = subprocess.run(['which', 'fabric'], capture_output=True, text=True)
        print(f'$ which fabric: {result.stdout}')
        return result.stdout.strip() != ''
    except Exception as e:
        print(f"Error checking Fabric availability: {e}")
        return False
    
def load_env_variables(env_file_path):
    """Load environment variables from a file."""
    env_vars = {}
    if os.path.exists(env_file_path):
        with open(env_file_path) as env_file:
            for line in env_file:
                if line.strip() and not line.startswith('#'):
                    key, value = line.strip().split('=', 1)
                    env_vars[key] = value
    print("Loading Fabric configuration:")
    print(env_vars)
    return env_vars

def call_fabric_command(command, text_input, *args):
    """Call a Fabric CLI command with the provided arguments and text input."""
    try:
        # Load the environment variables from the .env file
        env_file_path = os.path.expanduser('~/.config/fabric/.env')
        env_vars = load_env_variables(env_file_path)

        # Update the current environment with the loaded variables
        env = os.environ.copy()
        env.update(env_vars)

        cmd = ['fabric', command] + list(args)
        print(cmd)
        result = subprocess.run(cmd, input=text_input, capture_output=True, text=True, env=env, shell=False)
        if result.returncode != 0:
            print(f"Error calling Fabric command: {result.stderr}")
            return None
        return result.stdout
    except Exception as e:
        print(f"Error calling Fabric command: {e}")
        return None
    
def check_helper_tools():
    """Check if the required helper tools for Fabric are available."""
    possible_tools = ['yt', 'ts']
    available_tools = []
    
    for tool in possible_tools:
        try:
            result = subprocess.run(['which', tool], capture_output=True, text=True)
            print(result)
            if result.returncode == 0:
                available_tools.append(tool)
        except Exception as e:
            print(f"Fabric is missing helper tool `{tool}`: {e}")
    
    return available_tools

def download_youtube_transcript(youtube_url):
    """Download YouTube transcript using the 'yt' tool."""
    try:
        result = subprocess.run(['yt', '--transcript', youtube_url], capture_output=True, text=True)
        if result.returncode != 0:
            print(f"Error downloading YouTube transcript: {result.stderr}")
            return None
        return result.stdout.strip()
    except Exception as e:
        print(f"Error downloading YouTube transcript: {e}")
        return None

def is_valid_transcript(transcript):
    return len(transcript) < 100 and "Transcript not available in the selected language" not in transcript
    
def is_youtube_url(text):
    """Check if the text contains a YouTube URL."""
    return 'youtube.com' in text or 'youtu.be' in text

def is_youtube_video(youtube_url):
    checker_url = "https://www.youtube.com/oembed?url="
    video_url = checker_url + youtube_url

    response = requests.get(video_url)
    print(f'Youtube is video? {response.status_code == 200} - {response.elapsed.total_seconds()} seconds')

    return response.status_code == 200