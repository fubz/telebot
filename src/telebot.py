#!/usr/bin/env python
# pylint: disable=unused-argument
"""
Summarize Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Application and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic LLM Telegram bot example, summarizes messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging
import asyncio
from decouple import config
from telegram import BotCommand, ForceReply, Update
from telegram.ext import Application, CommandHandler, ContextTypes, MessageHandler, filters, PicklePersistence
from handler.errorHandler import error_handler
from handler.handlers import debug, help_command, fabric_command, llm, crew, dspySummarize
from db.db import connect_to_db

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", 
    level=logging.INFO
)
# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    await update.message.reply_html(
        rf"Hi {user.mention_html()}!",
        reply_markup=ForceReply(selective=True),
    )

async def test(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Test message."""
    await update.message.reply_text(update.message.text)
    
async def post_init(application: Application) -> None:
    commands = [
        BotCommand("start", "Check whether I am alive"),
        BotCommand("help", "Tips on using me"),
        BotCommand("fabric", "Run Fabric AI command")
    ]
    
    await application.bot.set_my_commands(commands)

async def main() -> None:
    """Start the bot."""
    file_persistence = PicklePersistence(filepath='../.bot/data', single_file=False )

    db_pool = await connect_to_db()

    print(file_persistence)

    # Create the Application and pass it your bot's token.
    application = Application.builder().token(config('TELEGRAM_BOT_TOKEN')).persistence(persistence=file_persistence).post_init(post_init).build()

    # on different commands - answer in Telegram
    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("help", help_command))
    application.add_handler(CommandHandler("fabric", fabric_command))

    # on non command i.e message - send to llm
    application.add_handler(MessageHandler(~filters.COMMAND & (filters.CaptionEntity("url") | filters.CaptionEntity("text_link") | filters.Entity("url") |filters.Entity("text_link")), dspySummarize))
    application.add_handler(MessageHandler(None, debug))

    # Error handling
    # application.add_error_handler(error_handler)

    # Store the db_pool in the application context
    application.db_pool = db_pool

    # Start the bot
    await application.initialize()
    await application.start()
    await application.updater.start_polling()

    # Run the bot until the user presses Ctrl-C
    # application.run_polling(allowed_updates=Update.ALL_TYPES)

    # Run the event loop until manually stopped
    try:
        await asyncio.Event().wait()
    finally:
        # Gracefully shut down
        await application.updater.stop()
        await application.stop()
        await application.shutdown()
        await db_pool.close()

if __name__ == '__main__':
    asyncio.run(main())