import asyncpg
import asyncio
from decouple import config

async def connect_to_db():
    # Telegram is in UTC and this must match
    timezone = 'UTC'  

    db_pool = await asyncpg.create_pool(
        user=config('DB_USER'),
        password=config('DB_PASSWORD'),
        database=config('DB_NAME'),
        host=config('DB_HOST')
    )
    return db_pool

class DatabaseConnection:
    _instance = None

    @staticmethod
    async def get_instance():
        if not DatabaseConnection._instance:
            # Create a connection pool instance only if it doesn't exist
            DatabaseConnection._instance = await connect_to_db()
    
        return DatabaseConnection._instance

    @staticmethod
    async def close_connection():
        db_pool = await DatabaseConnection.get_instance()
        await db_pool.close()


async def main():
    # Get the instance of db_pool whenever you need it
    db_pool = await DatabaseConnection.get_instance()
    # Perform database operations here using db_pool
    await DatabaseConnection.close_connection()


if __name__ == "__main__":
    asyncio.run(main())
