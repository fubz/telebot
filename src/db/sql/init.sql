CREATE TABLE messages (
    id SERIAL PRIMARY KEY,
    from_id BIGINT NOT NULL,
    peer_id BIGINT NOT NULL,
    post_author BIGINT NOT NULL,
    grouped_id BIGINT,
    date TIMESTAMP NOT NULL,
    message TEXT NOT NULL,
    post BOOL NOT NULL DEFAULT FALSE,
    from_scheduled BOOL NOT NULL DEFAULT FALSE,
    legacy BOOL NOT NULL DEFAULT FALSE,
    offline BOOL NOT NULL DEFAULT FALSE,
    processed BOOL NOT NULL DEFAULT FALSE
);

CREATE TABLE message_entities (
    id SERIAL PRIMARY KEY,
    message_id INTEGER REFERENCES messages(id),
    entity_length INTEGER,
    entity_offset INTEGER,
    entity_type VARCHAR(50),
    url TEXT
);