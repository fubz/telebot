from datetime import timedelta
import logging
from db.db import DatabaseConnection

logger = logging.getLogger(__name__)

async def insert_message(message):
    pool = await DatabaseConnection.get_instance()
    
    async with pool.acquire() as conn:
        async with conn.transaction():

            # Get the timezone information of the connection
            # tzinfo = conn._effective_timezone
            
            # Print the timezone information
            # print(f"Connection Timezone: {tzinfo}")

            result = await conn.fetchrow('''
                INSERT INTO messages (
                    id,
                    from_id, 
                    peer_id, 
                    post_author,
                    grouped_id,
                    
                    date, 
                    
                    message,
                    
                    post, 
                    from_scheduled, 
                    legacy, 
                    offline 
                ) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) 
                RETURNING id
            ''', 
            message.id,
            getattr(message, 'from_id.user_id', None),
            getattr(message, 'peer_id.user_id', None),
            message.post_author,
            message.grouped_id,
            message.date, 
            message.message,
            message.post, 
            message.from_scheduled, 
            message.legacy, 
            message.offline, 
            )

            message_id = result['id']

            for entity in message.entities:
                logger.debug("Entity: ")
                logger.debug(entity.stringify())
                logger.debug(entity.__class__.__name__)
                if(entity.__class__.__name__ in ["MessageEntityTextUrl", "MessageEntityUrl"]):
                    await conn.execute('''
                        INSERT INTO message_entities (
                            message_id, entity_length, entity_offset, entity_type, url
                        ) VALUES($1, $2, $3, $4, $5)
                    ''', 
                    message_id, 
                    entity.length, 
                    entity.offset, 
                    entity.__class__.__name__,
                    getattr(entity, 'url', None))
                
            return message_id
                

async def message_processed(message_id):
    pool = await DatabaseConnection.get_instance()
    async with pool.acquire() as conn:
        async with conn.transaction():
            await conn.execute('''
                UPDATE messages
                SET processed = TRUE
                WHERE id = $1
            ''', message_id)

async def select_messages_within(interval):
    pool = await DatabaseConnection.get_instance()
    async with pool.acquire() as conn:
        async with conn.transaction():
            query = '''
                SELECT * FROM messages
                WHERE post_author IS NOT NULL
                AND date >= localtimestamp - $1::interval
            '''
            results = await conn.fetch(query, interval)
            return results

def format_message(message):
    author = message['post_author']
    date = message['date'].strftime('%Y-%m-%d %H:%M:%S')
    content = message['message']
    return f"Author: {author}\nDate: {date}\nContent: {content}\n"

async def main():
    interval = timedelta(hours=8)
    messages = await select_messages_within(interval)

    message_text = "\n".join([format_message(message) for message in messages])
    
    print(message_text)

if __name__ == '__main__':
    import asyncio
    asyncio.run(main())