import json
from pathlib import Path
from haystack.components.builders import DynamicChatPromptBuilder, PromptBuilder
from haystack_integrations.components.generators.ollama import OllamaChatGenerator, OllamaGenerator
from haystack.components.generators.utils import print_streaming_chunk

from haystack.dataclasses import ChatMessage
from llm.haystack import Pipeline

# no parameter init, we don't use any runtime tempalte variables
prompt_builder = DynamicChatPromptBuilder()
generator = OllamaChatGenerator(
    model="llama2-uncensored:7b-chat-q2_K",
    generation_kwargs={
        "temperature": 0.1
    }
)


pipe = Pipeline()
pipe.add_component("prompt_builder", prompt_builder)
pipe.add_component("llm", generator)
pipe.connect("prompt_builder.prompt", "llm.messages")

clickbait_prompt = "Based on the article, provide a clickbait rating in dispaly in the format `Clickbait: [High, Medium, Low]`."

summarizeMessages = [
    ChatMessage.from_system("Condense the the user messages by shortening and summarizing the content without losing important information. Be sure to include List site_name, author, date. The summary must not be longer than the original content."),
    # ChatMessage.from_user("Please provide a summary of my messages."),
    ChatMessage.from_user("{{content}}"),
]   

pipe.draw(Path("./pipe.png"))

def summarizeContent(content):
    result = pipe.run(data={
        "prompt_builder": {
            "template_variables": {
                "content": content
            },
            "prompt_source": summarizeMessages,
        }})
    
    chatMessages = result["llm"]["replies"]
    wholeMessage = ""
    for message in chatMessages:
        print(message)
        wholeMessage += message.content + "\n"

    return wholeMessage





### Chat Tool Example
# react_prompt = """
# In the following conversation, a human user interacts with an AI Agent. The human user poses questions, and the AI Agent goes through several steps to provide well-informed answers.
# If the AI Agent knows the answer, the response begins with "Final Answer:" on a new line.
# If the AI Agent is uncertain or concerned that the information may be outdated or inaccurate, it must use the available tools to find the most up-to-date information. The AI has access to these tools:
# {tool_names_with_descriptions}

# The following is the previous conversation between a human and an AI:
# {memory}

# AI Agent responses must start with one of the following:

# Thought: [AI Agent's reasoning process]
# Tool: {tool_names} (on a new line) Tool Input: [input for the selected tool WITHOUT quotation marks and on a new line] (These must always be provided together and on separate lines.)
# Final Answer: [final answer to the human user's question]
# When selecting a tool, the AI Agent must provide both the "Tool:" and "Tool Input:" pair in the same response, but on separate lines. "Observation:" marks the beginning of a tool's result, and the AI Agent trusts these results.

# The AI Agent should not ask the human user for additional information, clarification, or context.
# If the AI Agent cannot find a specific answer after exhausting available tools and approaches, it answers with Final Answer: inconclusive

# Question: {query}
# Thought:
# {transcript}
# """


# messages = [
#     ChatMessage.from_system(react_prompt),
#     ChatMessage.from_user("{{content}}"),
#             ]

# def rag_pipeline_func(query: str):
#     result = rag_pipe.run({"embedder": {"text": query}, "prompt_builder": {"question": query}})

#     return {"reply": result["llm"]["replies"][0]}


# WEATHER_INFO = {
#     "Berlin": {"weather": "mostly sunny", "temperature": 7, "unit": "celsius"},
#     "Paris": {"weather": "mostly cloudy", "temperature": 8, "unit": "celsius"},
#     "Rome": {"weather": "sunny", "temperature": 14, "unit": "celsius"},
#     "Madrid": {"weather": "sunny", "temperature": 10, "unit": "celsius"},
#     "London": {"weather": "cloudy", "temperature": 9, "unit": "celsius"},
#     "Attica,  NY": {"weather": "rainy", "temperature": 42, "unit": "farenheight"},
#     "Caudersport,  PA": {"weather": "snowy", "temperature": 0, "unit": "farenheight"},
# }


# def get_current_weather(location: str):
#     if location in WEATHER_INFO:
#         return WEATHER_INFO[location]

#     # fallback data
#     else:
#         return {"weather": "sunny", "temperature": 21.8, "unit": "fahrenheit"}
    
# tools = [
#     {
#         "type": "function",
#         "function": {
#             "name": "rag_pipeline_func",
#             "description": "Get information about where people live",
#             "parameters": {
#                 "type": "object",
#                 "properties": {
#                     "query": {
#                         "type": "string",
#                         "description": "The query to use in the search. Infer this from the user's message. It should be a question or a statement",
#                     }
#                 },
#                 "required": ["query"],
#             },
#         },
#     },
#     {
#         "type": "function",
#         "function": {
#             "name": "get_current_weather",
#             "description": "Get the current weather",
#             "parameters": {
#                 "type": "object",
#                 "properties": {
#                     "location": {"type": "string", "description": "The city and state, e.g. San Francisco, CA"}
#                 },
#                 "required": ["location"],
#             },
#         },
#     },
# ]

# messages = [
#     ChatMessage.from_system(
#         "Don't make assumptions about what values to plug into functions. Ask for clarification if a user request is ambiguous."
#     ),
#     ChatMessage.from_user("Can you tell me where fubz lives?"),
# ]

# chat_generator = OllamaChatGenerator(model="llama2-uncensored:7b-chat-q2_K")
# response = chat_generator.run(messages=messages)

# print(response)

## Parse function calling information
# function_call = json.loads(response["replies"][0].content)[0]
# function_name = function_call["function"]["name"]
# function_args = json.loads(function_call["function"]["arguments"])
# print("Function Name:", function_name)
# print("Function Arguments:", function_args)

## Find the correspoding function and call it with the given arguments
# available_functions = {"rag_pipeline_func": rag_pipeline_func, "get_current_weather": get_current_weather}
# function_to_call = available_functions[function_name]
# function_response = function_to_call(**function_args)
# print("Function Response:", function_response)

# response = None


# while True:
#     # if OpenAI response is a tool call
#     if response and response["replies"][0].meta["finish_reason"] == "tool_calls":
#         function_calls = json.loads(response["replies"][0].content)

#         for function_call in function_calls:
#             ## Parse function calling information
#             function_name = function_call["function"]["name"]
#             function_args = json.loads(function_call["function"]["arguments"])

#             ## Find the correspoding function and call it with the given arguments
#             function_to_call = available_functions[function_name]
#             function_response = function_to_call(**function_args)

#             ## Append function response to the messages list using `ChatMessage.from_function`
#             messages.append(ChatMessage.from_function(content=json.dumps(function_response), name=function_name))

#     # Regular Conversation
#     else:
#         # Append assistant messages to the messages list
#         if not messages[-1].is_from(ChatRole.SYSTEM):
#             messages.append(response["replies"][0])

#         user_input = input("ENTER YOUR MESSAGE 👇 INFO: Type 'exit' or 'quit' to stop\n")
#         if user_input.lower() == "exit" or user_input.lower() == "quit":
#             break
#         else:
#             messages.append(ChatMessage.from_user(user_input))

#     response = chat_generator.run(messages=messages, generation_kwargs={"tools": tools})