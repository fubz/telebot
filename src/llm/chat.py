from haystack.components.builders import DynamicChatPromptBuilder
from haystack_integrations.components.generators.ollama import OllamaChatGenerator
from haystack.dataclasses import ChatMessage
from llm.haystack import Pipeline

# no parameter init, we don't use any runtime tempalte variables
prompt_builder = DynamicChatPromptBuilder()
generator = OllamaChatGenerator(
    model="llama2-uncensored:7b-chat-q2_K",
    generation_kwargs={
        "temperature": 0.7,
    }
)

pipe = Pipeline()
pipe.add_component("prompt_builder", prompt_builder)
pipe.add_component("llm", generator)
pipe.connect("prompt_builder.prompt", "llm.messages")
location = "Florida"
messages = [ChatMessage.from_system("You are a travel guide specialized in destinations for sexual swinger couples."),
            ChatMessage.from_user("Tell me about {{location}}")]

print(pipe.run(data={
    "prompt_builder": {
        "template_variables": {
            "location": location
        },
        "prompt_source": messages
        }}))