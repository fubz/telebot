from langchain_community.llms import Ollama
from langchain_community.chat_models import ChatOllama

from decouple import config
print('model ' + config("OPENAI_MODEL_NAME"))

llm = ChatOllama(
    model= config("OPENAI_MODEL_NAME"),
    temperature= 0.72,
)