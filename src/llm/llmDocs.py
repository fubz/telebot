import json
from pathlib import Path
from haystack.document_stores.in_memory import InMemoryDocumentStore
from haystack.components.retrievers.in_memory import InMemoryBM25Retriever, InMemoryEmbeddingRetriever
from haystack.components.preprocessors import DocumentCleaner, DocumentSplitter
from haystack.components.builders import DynamicChatPromptBuilder, PromptBuilder
from haystack.components.embedders import SentenceTransformersDocumentEmbedder
from haystack.components.converters import PyPDFToDocument
from haystack.components.writers import DocumentWriter
from haystack.document_stores.types import DuplicatePolicy
from haystack_integrations.components.generators.ollama import OllamaChatGenerator, OllamaGenerator
from haystack_integrations.components.embedders.ollama import OllamaDocumentEmbedder, OllamaTextEmbedder
from haystack.core.errors import PipelineConnectError
from haystack.components.generators.utils import print_streaming_chunk

from haystack.dataclasses import ChatMessage
from llm.haystack import Pipeline, Document

document_store = InMemoryDocumentStore(embedding_similarity_function="cosine")
embedder = OllamaDocumentEmbedder(model="mxbai-embed-large", url="http://localhost:11434/api/embeddings")
cleaner = DocumentCleaner(remove_empty_lines=True,	remove_extra_whitespaces=True,	remove_repeated_substrings=False)
splitter = DocumentSplitter()
# file_converter = PyPDFToDocument()
writer = DocumentWriter(document_store=document_store, policy=DuplicatePolicy.OVERWRITE)

## This is only to index documents
indexing_pipeline = Pipeline()
# Add components to pipeline
indexing_pipeline.add_component("embedder", embedder)
# indexing_pipeline.add_component("converter", file_converter)
indexing_pipeline.add_component("cleaner", cleaner)
indexing_pipeline.add_component("splitter", splitter)
indexing_pipeline.add_component("writer", writer)

# Connect components in pipeline
# indexing_pipeline.connect("converter", "cleaner")
indexing_pipeline.connect("cleaner", "splitter")
indexing_pipeline.connect("splitter", "embedder")
indexing_pipeline.connect("embedder", "writer")

documents = [
    Document(content="My name is fubz and I live in Attica, NY."), 
    Document(content="fubz is a cool guy."),
    Document(content="fubz is 42 years old."),
    Document(content="Todd lives in a white house."),
    Document(content="Victoria is 12."),    
]

# document_store.write_documents(embedder.run(documents)["documents"])

indexing_pipeline.run({"cleaner": {"documents": documents}})

## Indexing complete

template = """
Answer the questions based on the given context.

Context:
{% for document in documents %}
    {{ document.content }}
{% endfor %}
Question: {{ question }}
Answer:
"""


## This is to create a RAG pipeline in which the user can retireve data
embedder2 = OllamaTextEmbedder(model="mxbai-embed-large", url="http://localhost:11434/api/embeddings")
retriever2 = InMemoryEmbeddingRetriever(document_store=document_store)
prompt_builder2 = PromptBuilder(template=template)

generator2 = OllamaGenerator(
    model="llama2-uncensored:7b-chat-q2_K",
    generation_kwargs={
        "temperature": 0.1,
    }
)

rag_pipe = Pipeline()
rag_pipe.add_component("embedder", embedder2)
rag_pipe.add_component("retriever", retriever2)
rag_pipe.add_component("prompt_builder", prompt_builder2)
rag_pipe.add_component("llm", generator2)

# print(embedder2)
# print(retriever2)
# print(prompt_builder2)
# print(generator2)

# Now, connect the components to each other

rag_pipe.connect("embedder.embedding", "retriever.query_embedding")
rag_pipe.connect("retriever.documents", "prompt_builder.documents")
rag_pipe.connect("prompt_builder", "llm")

rag_pipe.draw(Path("./rag_pipe.png"))


def consumeDocument(documents):
    indexing_pipeline.run({"cleaner": {"documents": documents}})


# indexing_pipeline.run({"cleaner", {"documents": Document(content="My name is Jean and I live in Paris.")}, True})
query = "Tell me some facts about fubz"
testresponse = rag_pipe.run({"embedder": {"text":query}, "prompt_builder": {"question": query}})
print('Test Response: ' + testresponse["llm"]["replies"][0])