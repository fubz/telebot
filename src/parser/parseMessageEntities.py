def parseMessageEntities(entities, message):
    """Parse the message entities."""
    print(f"==== {len(entities)} entities ====")
    urls = []
    for entity in entities:
        print(entity)
        if(entity.type.__eq__("url")):
            print(f"URL: {entity}")
            if(message.text):
                url = message.parse_entity(entity)
                urls.append(url)
            elif(message.caption):
                url = message.parse_caption_entity(entity)
                urls.append(url)
            else:
                print("No text or caption")
        if(entity.type.__eq__("text_link")):
            urls.append(entity.url)

    return urls