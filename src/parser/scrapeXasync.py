import aiohttp
import asyncio
import json
from playwright.async_api import async_playwright
from parser.parseTweet import parse_tweet

async def process_tweet_call(xhr):
    """Process a tweet background request"""
    data = await xhr.json()
    print(data)
    return data['data']['tweetResult']['result']

def process_profile_call(xhr):
    """Process a profile background request"""
    data = xhr.json()
    return data['data']['user']['result']

async def scrape_tweet(url: str) -> dict:
    """
    Scrape a single tweet page for Tweet thread e.g.:
    https://x.com/sunny051488/status/1774462546394931253
    Return parent tweet, reply tweets and recommended tweets
    """
    _xhr_calls = []

    async def intercept_response(response):
        """capture all background requests and save them"""
        # we can extract details from background requests
        if response.request.resource_type == "xhr":
            _xhr_calls.append(response)
        return response

    async with async_playwright() as pw:
        browser = await pw.chromium.launch(headless=False)
        context = await browser.new_context(viewport={"width": 1920, "height": 1080})
        page = await context.new_page()

        # enable background request intercepting:
        page.on("response", intercept_response)
        # go to url and wait for the page to load
        await page.goto(url)
        await page.wait_for_selector("[data-testid='tweet']")

        # find all tweet background requests:
        tweet_calls = [f for f in _xhr_calls if "TweetResultByRestId" in f.url]

        tasks = []
        for xhr in tweet_calls:
            tasks.append(asyncio.create_task(process_tweet_call(xhr)))

        results = await asyncio.gather(*tasks)  # Await all tasks and store their results in 'results'

    print("Tweet JSON:")
    print(results)
    
    if(len(results) > 1):
        print("Error -- Handle more than 1 tweet!!!!")
        
    
    tweet = f"""Author: {results[0]['core']['user_results']['result']['legacy']['name']}
Tweet Date: {results[0]['core']['user_results']['result']['legacy']['created_at']}

{results[0]['legacy']['full_text']}
"""
    print(tweet)
    return tweet


async def scrape_profile(url: str) -> dict:
    """
    Scrape a X.com profile details e.g.: https://x.com/Scrapfly_dev
    """
    _xhr_calls = []

    async def intercept_response(response):
        """capture all background requests and save them"""
        # we can extract details from background requests
        if response.request.resource_type == "xhr":
            _xhr_calls.append(response)
        return response

    async with async_playwright() as pw:
        browser = await pw.chromium.launch(headless=False)
        context = await browser.new_context(viewport={"width": 1920, "height": 1080})
        page = await context.new_page()

        # enable background request intercepting:
        page.on("response", intercept_response)
        # go to url and wait for the page to load
        await page.goto(url)
        await page.wait_for_selector("[data-testid='primaryColumn']")

        # find all tweet background requests:
        tweet_calls = [f for f in _xhr_calls if "UserBy" in f.url]

        tasks = []
        for xhr in tweet_calls:
            tasks.append(asyncio.create_task(process_profile_call(xhr)))

        results = await asyncio.gather(*tasks)  # Await all tasks and store their results in 'results'



    return results  # Return the processed results



async def main():
    tweet = await scrape_tweet("https://x.com/sunny051488/status/1774462546394931253")
    result = parse_tweet(tweet)
    print("==== tweet ====")
    print(tweet)
    print("==== result ====")
    print(result)

    #
