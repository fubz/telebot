from telegram import MessageOriginUser, MessageOriginHiddenUser, MessageOriginChat, MessageOriginChannel
import datetime

def parseMessageOrigin(message_origin):
    r"""
    Function to parse out Username or Title of Channel from MessageOrigin

    Args:
        message_origin (MessageOrigin): The origin of the message from `telegram.MessageOrigin`

    Returns:
        dict: A dictionary containing the author's information and the date. Keys are 'author' and 'date'.

    Example:
        >>> message_origin = MessageOriginUser(message_type='USER', message_date=datetime.datetime.now())
        >>> parseOrigin(message_origin)
        {'author': 'John Doe', 'date': '2024-04-03 23:14:00'}
    """
    try:
        author = {
            MessageOriginUser: lambda x: f'{x.sender_user.first_name} {x.sender_user.last_name}',
            MessageOriginHiddenUser: lambda x: f'{x.sender_user_name}',
            MessageOriginChat: lambda x: x.chat.title,
            MessageOriginChannel: lambda x: x.chat.title
        }[type(message_origin)](message_origin)
    except (AttributeError, TypeError):
        author = 'Unknown'

    date = message_origin.date.strftime('%Y-%m-%d %H:%M:%S')

    return {
        'author': author,
        'date': date
    }