import logging

from db.message import insert_message, message_processed

logger = logging.getLogger(__name__)

async def handle_event(event):
    logger.info("New message received: %s", event.message.message)
    
    # Log pertinent event.message details to postgres database
    logger.debug(event.stringify())
    message_id = await insert_message(event.message)
    print(f"Stored message {message_id} in database")
    
    # Extract message contents, urls, videos, and picture from the payload
    
    # Send message contents to another processing function
    await message_processed(message_id)
