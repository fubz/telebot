import json
import re
import asyncio
import logging
from crewai_tools import tool
import requests
from handler.handleUrl import httpExtraction
from parser.scrapeXasync import scrape_tweet
from extractnet import Extractor
from fake_useragent import UserAgent
ua = UserAgent()

twitter = re.compile(r'^https?://(?:www\.)?(twitter|x)\.com')

facebook = re.compile('https://www\.facebook\..*')

@tool("Scrape a Webpage")
def ExtractURL(url: str):
    """Extracts the content and information from a webpage given a url by scraping."""
    print("Extracting url... " + url)

    extraction = None
    try:      
        # Fetch the webpage contents
        page = requests.get(url, {'User-Agent': ua.random})

        if page.status_code == 200:
            extraction = Extractor().extract(page.text)
            try:
                extraction['date'] = extraction['date'].strftime('%Y-%m-%d %H:%M:%S')
            except Exception as e:
                print(e)
                print("cleansing failed... carry on.")

        
            extraction = json.dumps(str(extraction), skipkeys=True)

        else:
            print(f'Extraction error code: {page.status_code}')

            
        print("==== extraction ====")
        print(extraction)

    except Exception as e:
        extraction = f"Failed to extract URL %s: %s", url, str(e)
        logging.error(extraction)

    return extraction