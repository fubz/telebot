import asyncio
from langchain.tools import tool
import requests
from unstructured.partition.html import partition_html
from decouple import config
from crewai import Agent, Task, Crew, Process

from llm.ollama import llm
from fake_useragent import UserAgent
ua = UserAgent()

context_length = 32000


class ScrapeTool():

  @tool("Scrape website content")
  def scrape_and_summarize_website(url):
    """Useful to scrape and summarize a website content given a url"""
   
    response = requests.get(url, {'User-Agent': ua.random})
    elements = partition_html(text=response.text)
    content = "\n\n".join([str(el) for el in elements])
    content = [content[i:i + context_length] for i in range(0, len(content), context_length)]

    summaries = []
    for chunk in content:
        print("==== chunk ====")
        print(chunk)
        print("==== end chunk ====")
        agent = Agent(
            llm=llm,
            role='WebScraper',
            goal='Summarize the given the provided context chunk while retaining much of the critical details.',
            backstory="You're a webscaper. Your context is a chunk of text extracted from the html of a website. Ignore the filler, advertisements, and links to additional articles.  Focus on the main idea of the webpage.",
            allow_delegation=False,
            verbose=True
            )
        task = Task(
            agent=agent,
            description=f'You are being provided a chunk of text from the html of a webpage. Provide a well researched summarization of the context including all important details.\n\nCONTENT\n----------\n{chunk}',
            expected_output='The most relevant information in the context is:\t'
        )
        summary = task.execute()
        summaries.append(summary)

    all_summaries = "\n\n".join(summaries)
    print('all_summaries: ' + all_summaries)
    
    return all_summaries
  
if __name__ == '__main__':
  ScrapeTool.scrape_and_summarize_website('https://nypost.com/2024/04/04/media/catherine-herridge-to-break-silence-at-capitol-hill-hearing/')