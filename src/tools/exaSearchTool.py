import os
from exa_py import Exa
from langchain.agents import tool
from decouple import config
import logging

class ExaSearchTool:

	exa = None

	@tool
	def search(query: str):
		"""Search for a webpage based on the query."""
		logging.info(f"Web Search: {query}")
		print(ExaSearchTool._exa())
		try:
			results = ExaSearchTool._exa().search(query, use_autoprompt=True, num_results=3)
		except Exception as e:
			logging.error(f"Error searching for webpages: {e}")
			return None
		logging.info("Search completed successfully.")
		return results

	@tool
	def findSimilar(url: str):
		"""Search for webpages similar to a given URL. The url passed in should be a URL returned from `search`.
		"""
		return ExaSearchTool._exa().find_similar(url, num_results=3)

	@tool
	def getContents(ids: str) -> str:
		"""Get the contents of a webpage. The ids must be passed in as either a string or a list, a list of ids returned from `search`.
		"""
		ids = eval(ids)
		contents = str(ExaSearchTool._exa().get_contents(ids))
		logging.debug(contents)
		contents = contents.split("URL:")
		contents = [content[:4000] for content in contents]
		return "\n\n".join(contents)

	def tools():
		return [ExaSearchTool.search, ExaSearchTool.findSimilar, ExaSearchTool.getContents]

	def _exa():
		logging.info(f"Exa Api: {config('EXA_API_KEY')}")
		exa = Exa(api_key=config("EXA_API_KEY"))
		return exa