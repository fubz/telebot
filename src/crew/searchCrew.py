import asyncio
from decouple import config
from crewai import Agent, Task, Crew, Process
from langchain_community.llms import Ollama
from crewai_tools import ScrapeWebsiteTool

from tools.exaSearchTool import ExaSearchTool
from tools.scrapeAndSummarize import ScrapeTool
from llm.ollama import llm

print('model ' + config("OPENAI_MODEL_NAME"))
print('api base'+ config("OPENAI_API_BASE"))
print('api key'+ config("OPENAI_API_KEY"))



# Key Attributes for Customization
# Role: Specifies the agent's job within the crew, such as 'Analyst' or 'Customer Service Rep'.
# Goal: Defines what the agent aims to achieve, in alignment with its role and the overarching objectives of the crew.
# Backstory: Provides depth to the agent's persona, enriching its motivations and engagements within the crew.
# Tools: Represents the capabilities or methods the agent uses to perform tasks, from simple functions to intricate integrations.

search_agent = Agent(
    llm=llm,
    role="Search Agent",
    goal="User is interested in a topic, search the internet for more content.",
    backstory="Expert in finding related content and reccomending unique connections to topics.",
    tools=ExaSearchTool.tools(),
    verbose=True,
    allow_delegation=False,
    max_iter=9
)

analysis_agent = Agent(
    llm=llm,
    role="Analysis Agent",
    goal="Give the users question or topic, use the search_agent to provide your analysis on the context.",
    backstory="Finding ways to tie together information is your speciality.  Find nice relations to the topic the user to explore.",
    tools=ExaSearchTool.tools(),
    verbose=True,
    allow_delegation=False,
    max_iter=9
)

conspiracy_agent = Agent(
    llm=llm,
    role="Conspiracy Agent",
    goal="You have a conspiracy for everything and you'll provide a conspiracy on the topic.",
    backstory="Knows all the underworkings of the cabal and cannot wait to tell their secrets.",
    # tools=[ExtractURL, ScrapeWebsiteTool(), ScrapeTool.scrape_and_summarize_website],
    verbose=True,
    allow_delegation=False,
    # max_iter=2
)


search_task = Task(
    description="Perform an internet search on the following topic and retrieve the most relevant webpage using the given tool. \n\nTopic: {topic}",
    expected_output="A text summary of the information found on the most relevant webpage for the given topic, or a direct link to the webpage if preferred.",
    agent=search_agent,
    # async_execution=True
)

analyze_task = Task(
    description="The user has provided you with a topic or question.  Analyze the context and feel free to search the internet for more information.\nUser Context: {topic}", 
    expected_output="A well thought out and researched response to the context.", 
    agent=analysis_agent
    )

conspiracy_task = Task(
    description="""
Find facts with a unique twist on the topic. Focus on corruption, conspiracies, cabal, government coverups.
Topic: {topic}
""",
    expected_output="Inform the user of unsual connections to the topic.",
    agent=conspiracy_agent,
    # async_execution=True
)



crew = Crew(
  agents=[analysis_agent],
  tasks=[analyze_task],
  process=Process.sequential,  # Optional: Sequential task execution is default
  cache=True,
  share_crew=False,
  verbose=2,
  memory=False,
)

def askSearchCrew(topic):
    result = crew.kickoff(inputs={'topic': topic})
    print("==== Ask Search Crew ====")
    # print(result)
    message = f"""
    {search_task.output.exported_output}

    {analyze_task.output.exported_output}
---

"""
    print(message)
    return message


# def stepCallback(param1):
#     print("Step callback called with params:", param1)

#     return True

# def taskCallback(param1):
#     print("Task callback called with params:", param1)
#     return True

if __name__ == '__main__':

    def main():
        # Code that should run when the script is executed directly

        test_crew = Crew(
            agents=[ analysis_agent],
            tasks=[ analyze_task],
            process=Process.sequential,
            cache=True,
            share_crew=True,
            verbose=2,
            memory=True,
            embedder={
                "provider": "huggingface",
                "config": {
                    "model": "mixedbread-ai/mxbai-embed-large-v1", # https://huggingface.co/mixedbread-ai/mxbai-embed-large-v1
                }
            },
            # step_callback=stepCallback,
            # task_callback=taskCallback
        )

        result = test_crew.kickoff(inputs={
            'topic': 'Josh Dean, from who worked at Boeing, died.  How did he die? What are other details around his death.  What lawsuit was he in with Boeing about?'
            })
        print("==== search-crew result ====")
        message = f"""
 
    ---
    {analyze_task.output.exported_output}
---

---

"""
        print(message)


    # # Run the main function asynchronously using an event loop
    # asyncio.run(
    main()
        # )