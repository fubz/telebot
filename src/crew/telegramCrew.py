import asyncio
import pprint
from decouple import config
from crewai import Agent, Task, Crew, Process
from langchain_community.llms import Ollama
from crewai_tools import ScrapeWebsiteTool

from tools.scrapeAndSummarize import ScrapeTool
from llm.ollama import llm

print('model ' + config("OPENAI_MODEL_NAME"))
print('api base'+ config("OPENAI_API_BASE"))
print('api key'+ config("OPENAI_API_KEY"))



# Key Attributes for Customization
# Role: Specifies the agent's job within the crew, such as 'Analyst' or 'Customer Service Rep'.
# Goal: Defines what the agent aims to achieve, in alignment with its role and the overarching objectives of the crew.
# Backstory: Provides depth to the agent's persona, enriching its motivations and engagements within the crew.
# Tools: Represents the capabilities or methods the agent uses to perform tasks, from simple functions to intricate integrations.

title_agent = Agent(
    llm=llm,
    role="Title Agent",
    goal="Create a concise title that engages readers and makes them want to read more.",
    backstory="Expert in creating pointed concise titles.",
    tools=[],
    verbose=True,
    allow_delegation=False,
    # max_iter=2
)



summarize_website_agent = Agent(
    llm=llm,
    role="Scrape Website",
    goal="Extract the contents of a website by scraping.",
    backstory="You connect to websites and read their content.",
    tools=[
        ScrapeWebsiteTool(), 
        # ScrapeTool.scrape_and_summarize_website, 
        # ExtractURL
        ],
    verbose=True,
    allow_delegation=False,
    max_iter=4
)



# summarize_website_agent = Agent(
#     llm=llm,
#     role="Summarize Website Agent",
#     goal="Read a website (url) and condense the content by shortening and summarizing the information without losing important details. ",
#     backstory="You are a freedom love and truth seeker skilled in wading through information trying to find the important bits. You know websites tend to focus on more than one topic but you are trained to find the signal through the noise.",
#     tools=[ScrapeWebsiteTool(), ScrapeTool.scrape_and_summarize_website, ExtractURL],
#     verbose=True,
#     allow_delegation=False,
#     # max_iter=2
# )

customer_service = Agent(
    llm=llm,
    role="Customer Service",
    goal="Ask the user if they would like further information on the content. If so, ask teammates for an answer.",
    backstory="You are trained to find the signal through the noise. Be thoughtful and thorough in your work.",
    # tools=[ExtractURL, ScrapeWebsiteTool(), ScrapeTool.scrape_and_summarize_website],
    verbose=True,
    allow_delegation=False,
    # max_iter=2
)

clickbait_agent = Agent(
    llm=llm,
    role="Clickbait Agent",
    goal="Rate the content on it's use of clickbait tactics on a scale from 0-5. 0 being the content is void of clickbait. 5 being the content uses 5 or more clickbait tactics. Provide the number only with no explanation.",
    backstory="You dispise content designed to engage audiences yet provide no value.",
    tools=[
        # "Google Search Engine",
    ],
    verbose=True,
    memory=True,
    allow_delegation=False,
    # max_iter=2
)

webscraper = Agent(
    llm=llm,
    role="Web Scraper Agent",
    goal="Scrape a website for information and return the content. Attribute author if confident.",
    backstory="You dispise content designed to engage audiences yet provide no value.",
    verbose=True,
    memory=True,
    allow_delegation=False,  
)

title_task = Task(
    description="""
Create a 3 to 7 word title which captures the essence of this caption & content: 
caption: {caption}
content: {content}
""",
    expected_output="The Final answer must be a 3 to 7 word title that includes at least 1 emoji.  Do not provide an explanation, reasoning, or additional data.",
    agent=title_agent,
    # async_execution=True
)

# summarize_task = Task(
#     description="Summarize this content: {caption} {content}",
#     expected_output="Condensed the content by highlighting important facts not being wordy.",
#     agent=summarize_agent,
#     # async_execution=True
# )

clickbait_task = Task(
    description="""You are an AI language model trained to evaluate the clickbait level of messages. Your task is to assess messages based on the following Clickbait Rating Scale from 0 to 5:

Clickbait Rating Scale:

0 - No Clickbait

The message is straightforward, factual, and devoid of any sensationalism or exaggeration.
Example: "Meeting at 3 PM tomorrow."
1 - Minimal Clickbait

The message contains slight intrigue or minor emotional triggers but remains mostly straightforward.
Example: "You might want to check this out."
2 - Low Clickbait

The message uses some curiosity-inducing elements or mildly exaggerated language but is still relatively clear.
Example: "This new tool could change how you work."
3 - Moderate Clickbait

The message employs several clickbait tactics, such as vague details, emotional triggers, or implied urgency.
Example: "You won't believe how easy this trick is to save money!"
4 - High Clickbait

The message heavily relies on sensationalized content, strong emotional triggers, and a sense of urgency or exclusivity.
Example: "Breaking news! This secret method is making people rich overnight!"
5 - Extreme Clickbait

The message is packed with multiple clickbait tactics, including sensationalism, extreme vagueness, high emotional triggers, and urgent calls to action.
Example: "Shocking! This one weird trick will change your life forever - don't miss out!"
Instructions:
Evaluate Content: Carefully read the provided message.
Identify Tactics: Identify any clickbait tactics used in the message.
Count Tactics: Note the number of clickbait tactics present.
Assign Rating: Assign a rating based on the scale, where more tactics correspond to a higher rating.
Example Usage:
Given the message: "You won't believe what happened today at the office!"

Evaluation: The message uses vague details, curiosity, and emotional triggers.
Rating: 3 (Moderate Clickbait)
Given the message: "Check out the latest project update."

Evaluation: The message is straightforward with minimal intrigue.

Rating: 1 (Minimal Clickbait)


Provide a Clickbait Rating for this message: 
{caption} {content}
""",
    expected_output="Provide the clickbait rating as an integer from 0 to 5 along with only a single sentence of why you assigned that rating.",
    agent=clickbait_agent,
    # async_execution=True
)

message_summarize_task = Task(
    description="""You receive Telegram messages containing a caption, message text, and optional URLs. When a URL is included, you will scrape the webpage and analyze its contents along with the original message text.

Objective:
Thoroughly summarize the key points from both the message and the scraped webpage content. Note that webpage content may contain information to unrelated topics thus it's important to focus on the content that is related to the Telegram Message.

Style:
Present the information with the precision and neutrality of unbiased journalism, but add a flair of an insatiable quest for the truth. Subtle humor should be included to make the summary more engaging.

Tone:
Keep it professional and informative, but don't shy away from witty remarks to lighten the mood.

Audience:
General public who values accurate and detailed information delivered in a clear and slightly entertaining manner.

Response:
Generate a summary that encapsulates the main ideas from the message and the webpage. Ensure the summary is clear, comprehensive, and engaging, with appropriate use of emojis to highlight key points.

Scrape this website: {url}

Telegram Message:
{caption} {content}
""",
    expected_output="Generate a summary that encapsulates the main ideas from the message and the webpage. Ensure the summary is clear, comprehensive, and engaging, with appropriate use of emojis to highlight key points. If unable to connect to the website due to tool errors respond with the error message only.",
    agent=summarize_website_agent,
    async_execution=False,
)

webscrape_task = Task(
    description="Scrape this website: {url}",
    expected_output="A summary of the content found on the website. If unable to connect to the website due to tool errors respond with the error message only.",
    agent=summarize_website_agent,
    async_execution=False,
    # tools=[ExtractURL, ScrapeWebsiteTool(), ScrapeTool.scrape_and_summarize_website]
)

happy_human = Task(
    description="Ask the user if they would like any followup information?",
    expected_output="Response to the users demands.",
    agent=customer_service,
    human_input=True,
)

# summarize_url = Task(
#     description="Extract the body of the webpage provide by the webscraper.  Note there may be irrelevant data that must be filtered out.",
#     expected_output="The true content of the webpage and not the advertisements, comments, and other nonrelevant content.",
#     agent=summarize_agent,
#     context=[webscrape_task]
# )

scraper_agent = Agent(
    llm=llm,
    role='Web Content Scraper',
    goal='Extract wisdom from provided URLs',
    verbose=True,
    memory=True,
    backstory=(
        "A detail-oriented analyst with a knack for uncovering valuable information "
        "from the depths of the internet. Your work ensures the team has the most accurate data."
    ),
    tools=[ScrapeWebsiteTool()],
    allow_delegation=False
)

summarizer_agent = Agent(
    llm=llm,
    role='Content Summarizer',
    goal='Generate clear, comprehensive, and engaging summaries from the given text and scraped content',
    verbose=True,
    memory=True,
    backstory=(
        "A seasoned journalist with a passion for presenting the truth in an engaging manner. "
        "You combine precision and neutrality with a flair for witty remarks, making the information "
        "accessible and entertaining for the general public."
    ),
    allow_delegation=False
)

# Define tasks
scraping_task = Task(
    description=(
    """
# IDENTITY and PURPOSE

You are an expert content summarizer. You take content in and output a concise, informative summary using the format below.

Take a deep breath and think step by step about how to best accomplish this goal using the following steps.

# OUTPUT SECTIONS

- Combine all of your understanding of the content into a single, 20-word sentence in a section called ONE SENTENCE SUMMARY:.

- Output the 10 most important points of the content as a list with no more than 15 words per point in a section called MAIN POINTS:.

- Output a list of the 5 best takeaways from the content, each highlighting a unique insight, in a section called TAKEAWAYS:.

# OUTPUT INSTRUCTIONS

- Create the output using the formatting above.
- Output numbered lists, not bullets.
- Ensure each point is clear, precise, and free of repetition.
- Prioritize technical terms and concepts, explaining them succinctly.
- Highlight the most crucial aspects like key features, unique advantages, and practical use cases.
- Do not start items with the same opening words.
- Output must begin with the line "Final Answer:"

# INPUT:
Use the tool to read a website content.

Read website content(website_url: '{url}')
"""
    ),
    expected_output="""Final Answer:
The content of the website_url summarized as requested. 
    """,
    tools=[ScrapeWebsiteTool()],
    agent=scraper_agent,
    output_variable='scraped_content'  # Define output variable to store result
)

summarization_task = Task(
    description=("""
# IDENTITY and PURPOSE

You are an expert content summarizer. You take content in and output a Markdown formatted summary using the format below.

Take a deep breath and think step by step about how to best accomplish this goal using the following steps.

# OUTPUT SECTIONS

- Combine all of your understanding of the content into a single, 20-word sentence in a section called ONE SENTENCE SUMMARY:.

- Output the 10 most important points of the content as a list with no more than 15 words per point into a section called MAIN POINTS:.

- Output a list of the 5 best takeaways from the content in a section called TAKEAWAYS:.

# OUTPUT INSTRUCTIONS

- Create the output using the formatting above.
- Output numbered lists, not bullets.
- Do not output warnings or notes—just the requested sections.
- Do not repeat items in the output sections.
- Do not start items with the same opening words.
- Output must begin with the line "Final Answer:"

# INPUT:

Telegram Message: {caption} {content}  
"""
    
    ),
    expected_output='A comprehensive and engaging summary of the telegram message and webscrape content.',
    agent=summarizer_agent,
    async_execution=False,
)

crew = Crew(
    agents=[title_agent, clickbait_agent, scraper_agent, summarize_website_agent],
    tasks=[title_task, clickbait_task, scraping_task, summarization_task],
    process=Process.sequential,  # Optional: Sequential task execution is default
    cache=True,
    share_crew=False,
    verbose=3,
    # memory=True,
    # embedder={
    #     "provider": "huggingface",
    #     "config": {
    #         "model": "mixedbread-ai/mxbai-embed-large-v1", # https://huggingface.co/mixedbread-ai/mxbai-embed-large-v1
    #     }
    # },
)

def askTelegramCrew(caption, text, urls):
    result = crew.kickoff(inputs={'content': text, 'url': urls, 'caption': caption})
    print("==== Ask Telegram Crew ====")
    print(clickbait_task.output)
    message = f"""
    {title_task.output.exported_output}
---
    {summarization_task.output.exported_output}
---
    Clickbait Rating: {clickbait_task.output.raw_output}
"""
    print(message)
    return message, title_task.output.exported_output, summarization_task.output.exported_output, clickbait_task.output.exported_output

def inspect_tools(agent):
    for tool in agent.tools:
        print(f"Tool Name: {tool.name}")
        print(f"Tool Description: {tool.description}")

def step_callback(agent_action):
    print("step_callback")
    pprint.saferepr(agent_action)

def task_callback(task_info):
    print("task_callback")
    pprint.pprint(task_info)
    
# def taskCallback(task_result, task, agent):
#     print(f"Task completed: {task.description} by agent {agent.role}")
#     print(f"Final result:")
#     pprint.pp(task_result)
    
#     print("Tools used by the agent:")
#     inspect_tools(agent)
#     return True

if __name__ == '__main__':

    async def main():
        # Code that should run when the script is executed directly
        for tool in scraper_agent.tools:
            print(f"Tool Name: {tool.name}, Tool Description: {tool.description}")

        test_crew = Crew(
            agents=[scraper_agent],
            tasks=[scraping_task],
            process=Process.sequential,
            cache=True,
            share_crew=False,
            verbose=2,
            memory=False,
            embedder={
                "provider": "huggingface",
                "config": {
                    "model": "mixedbread-ai/mxbai-embed-large-v1", # https://huggingface.co/mixedbread-ai/mxbai-embed-large-v1
                }
            },
            step_callback=step_callback,
            task_callback=task_callback
        )

#         result = test_crew.kickoff(inputs={
#             'url': 'https://zed.dev/blog/zed-decoded-async-rust',
#             'caption': None,
#             'content': """
# Welcome to the first article in a new series called Zed Decoded. In Zed Decoded I'm going to take a close look at Zed — how it's built, which data structures it uses, which technologies and techniques, what features it has, which bugs we ran into. The best part? I won't do this alone, but get to interview and ask my colleagues here at Zed about everything I want to know.
# """
#             })

        result = test_crew.kickoff(inputs={
            'url': 'https://badlands.substack.com/p/q-for-dummies-chapter-14',
            'caption': None,
            'content': """
The Q for Dummies series continues today on Badlands.

Whether you're a newbie or an OG Anon, it's never too late to brush up on the most influential OSINT operation of all time.
"""
            })
        
        
        print("==== result ====")
        print(f"""
              {scraping_task.output.exported_output}
              """)


    # # Run the main function asynchronously using an event loop
    asyncio.run(main())