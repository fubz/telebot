from datetime import timedelta
import logging
import sys
import re
import threading
from decouple import config
from telethon import TelegramClient, __version__, events
from db.db import DatabaseConnection
from db.message import format_message, select_messages_within
from event_handler import handle_event
from handler.fabricHandler import call_fabric_command, download_youtube_transcript, is_valid_transcript, is_youtube_url
from handler.dspy_summary import dspy_summary



print(f'Telethon Version used: {__version__}')

CHANNELS = config('CHANNELS').split(',')

CHANNEL_EXCLUSIONS = [-1001272690921, -1001483766496]

# https://my.telegram.org/apps
api_id = config('TELEGRAM_API_ID')
api_hash = config('TELEGRAM_HASH')
client = TelegramClient('aiBot', api_id, api_hash, sequential_updates=True)


def parse_catchup(command):
    match = re.match(r"!catchup(?:\s+(\d+))?", command)
    if match:
        hours = int(match.group(1)) if match.group(1) else 4  # Default to 4 hours if no number is provided
        return hours
    return None

async def main():
    
    await client.start()
    
    # Getting information about yourself
    me = await client.get_me()

    # "me" is a user object. You can pretty-print
    # any Telegram object with the "stringify" method:
    print(me.stringify())

    # When you print something, you see a representation of it.
    # You can access all attributes of Telegram objects with
    # the dot operator. For example, to get the username:
    username = me.username
    print(username)
    print(me.phone)

    # You can print all the dialogs/conversations that you are part of:
    async for dialog in client.iter_dialogs(archived=False):
        print(dialog.name, 'has ID', dialog.id)
        if(dialog.name in ['Example']):
            # print(dialog.name, 'has ID', dialog.id)
            print(dialog.stringify())
        

    async def periodic_task(hours):
        while True:
            await hourly_update(hours)
            await asyncio.sleep(hours * 3600)  # Sleep for X hours
        
    async def hourly_update(hours=3):
        print(f"Running hourly update: {hours}")
        interval = timedelta(hours=hours)
        messages = await select_messages_within(interval)
        print(f"Found {len(messages)} messages")

        if len(messages) > 0:
            try:
                message_text = "\n".join([format_message(message) for message in messages])
                # response = await dspy_summary(message_text)
                response = call_fabric_command("-p", message_text, "summarize")
                m = await client.send_message('me', response)
            except Exception as e:
                print(e)
        else:
            print(f"No messages to process in the last {hours} hours")
        
            
    print("Bot started and listening to channels: %s", CHANNELS)        
    @client.on(events.NewMessage(chats=CHANNELS))
    async def channel_listener(event):
        await handle_event(event)
    


    @client.on(events.NewMessage(pattern=r".*(https?://\S*?(youtube\.com|youtu\.be)/\S+)"))
    async def youtube(event):
        print('Youtube')
        print(event.chat_id)

        if event.chat_id not in CHANNEL_EXCLUSIONS:
            print("Got youtube link")
            match = re.match(r".*(https?://\S*?(youtube\.com|youtu\.be)/\S+)", event.raw_text)
            if match:
                try:
                    url = match.group(1)
                    print(f"Received a youtube URL: {url}")

                    if is_youtube_url(url):
                        transcript = download_youtube_transcript(url)
                        print("Transcript:")
                        print(transcript)

                        if is_valid_transcript(transcript):
                            print("Youtube Transcript not long enough to summarize, probably invalid.")
                        else :
                            # response = await dspy_summary(f"{transcript}")
                            # print(response)
                            # m = await event.reply(response.summary)
                            # print(m)
                            fabric = call_fabric_command("-p", f"{transcript}", "extract_wisdom")
                            m = await event.reply(fabric)
                        
                except Exception as e:
                    print(e)
                    
                
            else:
                print("No youtube link found", match)
                # await event.respond("Invalid format. Use !yt <URL>")
        else:
            print("Excluded channel sent youtube link")
        
    @client.on(events.NewMessage(pattern=r"!catchup(?:\s+\d+)?"))
    async def catchup(event):
        try:
            hours = parse_catchup(event.raw_text)
            print(event.pattern_match)
            await client.edit_message(event.message, f"Gathering the last {hours} hours of news...")

            await hourly_update(hours=hours)
        except Exception as e:
            print(e)
        
    
    @client.on(events.NewMessage(pattern='!ping'))
    async def handler(event):
        # Say "!pong" whenever you send "!ping", then delete both messages
        m = await event.respond('!pong')
        await asyncio.sleep(5)
        await client.delete_messages(event.chat_id, [event.id, m.id])


    # Start the periodic task
    asyncio.create_task(periodic_task(1))
    
    await client.run_until_disconnected()
    
    
if __name__ == '__main__':
    import asyncio
    asyncio.run(main())
