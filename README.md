

# Getting started

## Requirements

- Python 3.10
- Local Ollama installation
    - https://ollama.com/download
- Ollama model
    - Recommended: https://ollama.com/library/mistral:instruct
- Optional:
    - Fabric AI
        - https://github.com/danielmiessler/fabric?tab=readme-ov-file#quickstart
    - Fabric Helper Apps
        - https://github.com/danielmiessler/fabric?tab=readme-ov-file#helper-apps
        - `yt` is highly reccomended as it allows you to summarize youtube videos

Ensure Fabric AI is available before running the bot:
```shell
which fabric
```

## Configure the Bot

Create a file named `.env` alongside this readme.md

```.env
PASSWORD=another uneeded password for openapi since we use ollama
TELEGRAM_BOT_TOKEN="Message https://t.me/BotFather on telegram to configure your bot"
OPENAI_API_BASE='http://localhost:11434/v1'
OPENAI_MODEL_NAME='mistral:7b-instruct-v0.2-fp16'
OPENAI_API_KEY='not_needed_for_ollama'
CONTEXT_LENGTH=4000
EXA_API_KEY=api key for exasearch

DB_HOST=127.0.0.1
DB_NAME=telebot_db
DB_USER=telebot_user
DB_PASSWORD=telebot_password
```

## Run the Bot

```shell
# Install Python Stuff
python -m  venv mybot
source mybot/bin/activate
pip install -r requirements.txt

# Run the bot
cd src
python -m telebot
```

# Bot Usage

Bot will summarize messages it receives and will also scrape websites linked in messages.


## Fabric Bot commands

All flags are passed to fabric cli


examples:
```
/fabric -p extract_wisdom But of all the things the ancients left us, I believe the best thing was their words of wisdom.
/fabric -l
/fabric --listmodels
/fabric -p extract_wisdom https://www.youtube.com/watch?v=dQw4w9WgXcQ
```

# Telegram Bot Objectives

Bot will be able to receive and process messages from Telegram channel or Forwards
Bot will reply with summarization of the message and it's attachments.
On a recurring period the bot will provide a look back summarization of the messages.

## Bot Flow

1. Consume messages from Telegram channel or Forwards
2. For each message:
    - Store message in database
    - Parse message for actionable information:
        - Caption, Message, Media (image/video), URLs
    - Caption/Message Text (Distinctly different in telegram but used interchangeably)
        - Summarize with LLM
        - Send to Clickbait Rating Agent
    - Images:
        - Send to img2txt
    - URLs:
        - Scrape the page and extract: Title, Content, Author, Date
        - Store extracts in database
        - Summarize content with LLM
    - VectorDB?
4. Reply with:
    - Message Summary
    - Image Captioning
    - URL Scraped Content Summary
    - Clickbait Rating
3. For each Day period at X time
    - Send daily summary of all messages sent to channel/Fwd
    - Sentiment Analysis
    - Theme Analysis



